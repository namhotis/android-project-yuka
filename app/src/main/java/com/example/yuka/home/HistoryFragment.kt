package com.example.yuka.home

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.yuka.History
import com.example.yuka.Product
import com.example.yuka.R
import com.example.yuka.details.DetailActivity
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list.*
import kotlinx.android.synthetic.main.list_item.*
import kotlinx.android.synthetic.main.list_item.view.*

class ListeDernieresRecherches : Fragment(), OnProductClickListener {


    var ref : DatabaseReference? = null
    var listener : ValueEventListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        retainInstance = true;
        return inflater.inflate(R.layout.list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true;
        recyclerview.layoutManager = LinearLayoutManager(requireContext())


    }
    override fun onResume() {
        super.onResume()
        ref = FirebaseDatabase.getInstance().getReference("User/user_0/historique")
        listener = ref!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val history = mutableListOf<History>()

                for (hist in dataSnapshot.children) {
                    val element = hist.getValue(History::class.java)
                    if (element != null) {
                        history.add(element)
                    }
                }
                val products = history.map { it.toProduct() }
                recyclerview.adapter = ListAdapter(products, this@ListeDernieresRecherches)
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
    override fun onPause() {
        super.onPause()

        if (listener != null) {
            ref?.removeEventListener(listener!!)
        }
    }

    override fun onProductClicked(product: Product) {
        val intent = Intent(activity?.applicationContext, DetailActivity::class.java)
        intent.putExtra("barcode", product.codeBarre)
        startActivity(intent)
    }

    override fun onBookmarkClicked(product: Product) {
        product_bookmark.setColorFilter(0xFF007100.toInt(), PorterDuff.Mode.SRC_ATOP)

    }


}


class ListAdapter(val products: List<Product>, val listener: OnProductClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = products.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductCell(inflater.inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductCell).bindProduct(products[position], listener)
    }

}

class ProductCell(v: View) : RecyclerView.ViewHolder(v) {
    val productNameView: TextView = v.product_name
    val productMarqueView: TextView = v.product_brand
    val cardview: CardView = v.product_item_card
    var bookmark: ImageView = v.product_bookmark
    val productImageView: ImageView = v.product_item_image
    val productCaloriesView: TextView = v.product_calories
    val productNutriscoreView: TextView = v.product_nutriscore

    fun bindProduct(product: Product, listener: OnProductClickListener) {
        productNameView.text = product.nomRecette
        Picasso.get().load(product.url).into(productImageView)
        var resultMarques = product.marque.toString().replace("[", "").replace("]", "")
        productMarqueView.text = resultMarques
        /*Picasso.get().load(product.nutriscore).into(productNutriscoreView)*/
        when (product.nutriscore) {
            "a" -> productNutriscoreView.text = "Nutriscore: A"
            "b" -> productNutriscoreView.text = "Nutriscore: B"
            "c" -> productNutriscoreView.text = "Nutriscore: C"
            "d" -> productNutriscoreView.text = "Nutriscore: D"
            "e" -> productNutriscoreView.text = "Nutriscore: E"
        }

        cardview.setOnClickListener {
            listener.onProductClicked(product)
        }

        bookmark.setOnClickListener {
            listener.onBookmarkClicked(product)
        }
    }
}




interface OnProductClickListener {
    fun onProductClicked(product: Product)
    fun onBookmarkClicked(product: Product)
}

