package com.example.yuka.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.yuka.details.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(com.example.yuka.R.layout.activity_main)

        supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this, com.example.yuka.R.drawable.toolbar))
        supportActionBar?.setTitle(com.example.yuka.R.string.toolbar_title)

        main_navigation.setOnNavigationItemSelectedListener {

            val fragment = when (it.itemId) {
                com.example.yuka.R.id.main_navbar_history -> ListeDernieresRecherches()
                com.example.yuka.R.id.main_navbar_favs -> FavoritesProducts()
                com.example.yuka.R.id.main_navbar_stats -> Statistiques()
                com.example.yuka.R.id.main_navbar_profile -> Profils()
                else -> throw Exception("")
            }

            val title = when (it.itemId) {
                com.example.yuka.R.id.main_navbar_history -> "Mon historique"
                com.example.yuka.R.id.main_navbar_favs -> "Mes favoris"
                com.example.yuka.R.id.main_navbar_stats -> "Statistiques"
                com.example.yuka.R.id.main_navbar_profile -> "Mon profil"
                else -> throw Exception("")
            }

            supportActionBar?.title = title

            supportFragmentManager.beginTransaction()
                .replace(com.example.yuka.R.id.main_content, fragment)
                .commitAllowingStateLoss()

            true
        }

        main_navigation.selectedItemId = com.example.yuka.R.id.main_navbar_history
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        Toast.makeText(this@MainActivity, "Its toast!", Toast.LENGTH_SHORT).show()
        val intent = Intent("com.google.zxing.client.android.SCAN")
        intent.putExtra("SCAN_FORMATS", "EAN_13");
        super.startActivityForResult(intent, 1235)

        return super.onOptionsItemSelected(item)
    }


    /* override fun startActivityForResult(intent: Intent?, requestCode: Int) {
         super.startActivityForResult(intent, requestCode)
     }
  */

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(com.example.yuka.R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null) {
            val resultFormat: String = data.getStringExtra("SCAN_RESULT_FORMAT")
            val scanResult: String = data.getStringExtra("SCAN_RESULT")
            Toast.makeText(this@MainActivity, scanResult, Toast.LENGTH_SHORT).show()
            Toast.makeText(this@MainActivity, resultFormat, Toast.LENGTH_SHORT).show()
            super.onActivityResult(requestCode, resultCode, data)
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("barcode", scanResult)
            startActivity(intent)


        }

    }
}
