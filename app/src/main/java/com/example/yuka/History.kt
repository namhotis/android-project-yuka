package com.example.yuka

data class History(
    val nomRecette: String?,
    val marque: List<String?>?,
    val codeBarre: String?,
    val url: String?,
    val productNutriscore: String?
) {
    fun toProduct() : Product{
        return Product(
            nomRecette = this.nomRecette,
            marque = this.marque,
            codeBarre = this.codeBarre,
            url = this.url,
            nutriscore = this.productNutriscore
        )
    }

        constructor() : this("", null, null, null, null)

}