package com.example.yuka.details

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.yuka.NetworkManager
import com.example.yuka.Product
import com.example.yuka.R
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.detail_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(!intent.hasExtra("barcode")) {
            throw Exception("The product is missing")
        }

        setContentView(R.layout.detail_activity)

        initToolbar()

        val barcode = intent.getStringExtra("barcode")

        // Appeler l'API avec le barcode

        GlobalScope.launch(Dispatchers.Main) {

            viewpager.visibility = View.GONE
            progressbar.visibility = View.VISIBLE
            /*iew.showLoading()*/
            try {
                val product = withContext(Dispatchers.IO) { NetworkManager.getProduct(barcode) }

                if (product != null) {
                    if (savedInstanceState == null) {
                        addToHistory(product)
                    }

                    viewpager.adapter =
                        ProductsDetailsAdapter(supportFragmentManager, product)
                } else {
                    Toast.makeText(this@DetailActivity, "Ca marche pas lol", Toast.LENGTH_SHORT).show()
                }


                progressbar.visibility = View.GONE
                viewpager.visibility = View.VISIBLE
                tabs.setupWithViewPager(viewpager)

            } catch (e: Exception) { "" }
        }

        // Donner l'objet Product

    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        super.onSaveInstanceState(outState, outPersistentState)
        outState?.putBoolean("fake", true)
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar))
        supportActionBar?.setTitle(R.string.toolbar_detail_title)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class ProductsDetailsAdapter(fm: FragmentManager, private val product: Product) : FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> MyFicheProduct.newInstance(product)
                1 -> MyNutritionFragment.newInstance(product)
                2 -> MyFicheNutritionFragment.newInstance(product)
                else -> throw Exception("WTF")
            }
        }

        override fun getCount(): Int = 3

        override fun getPageTitle(position: Int): CharSequence?{
            return when (position) {
                0 -> "Fiche"
                1 -> "Nutrition"
                2 -> "Infos nutrionnelles"
                else -> {
                    throw Exception("WTF tab")
                }
            }
        }
    }

   private fun addToHistory(product: Product) {
        // product -> history
        val history = product.toHistory()

        //Insertion dans la BDD
        val database = FirebaseDatabase.getInstance()
        database.getReference("User/user_0/historique/${product.codeBarre}")
            .setValue(history, object : DatabaseReference.CompletionListener {

                override fun onComplete(databaseError: DatabaseError?, databaseReference: DatabaseReference) {}

            })
    }




}