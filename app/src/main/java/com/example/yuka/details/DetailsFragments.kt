package com.example.yuka.details

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.yuka.Product
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.main1.*


open class BaseFragment : Fragment() {

    companion object {
        val EXTRA_PRODUCT = "product"
    }

    lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments == null || arguments?.containsKey(EXTRA_PRODUCT) == false) {
            throw Exception("The product is missing")
        }

        product = arguments!!.getParcelable<Product>(EXTRA_PRODUCT)!!
    }
}

open class MyFicheProduct : BaseFragment() {

    companion object {
        fun newInstance(product: Product): MyNutritionFragment {
            val fragment = MyNutritionFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_PRODUCT, product)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.example.yuka.R.layout.main1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titreRecette.text = Html.fromHtml(getString(com.example.yuka.R.string.nomRecette, product.nomRecette))
        marqueRecette.text = Html.fromHtml(getString(com.example.yuka.R.string.marque, product.marque))
        codeBarre.text = Html.fromHtml(getString(com.example.yuka.R.string.codeBarre, product.codeBarre))
        quantite.text = Html.fromHtml(getString(com.example.yuka.R.string.quantite, product.quantite))
        lieuVente.text = Html.fromHtml(getString(com.example.yuka.R.string.lieuVente, product.lieuVente))
        ingredients.text = Html.fromHtml(getString(com.example.yuka.R.string.ingredients, product.ingredients))
        allergenes.text = Html.fromHtml(getString(com.example.yuka.R.string.allergenes, product.allergenes))
        additifs.text = Html.fromHtml(getString(com.example.yuka.R.string.additifs, product.additifs))
        titreRecette.text = product.nomRecette
    }

}

open class MyNutritionFragment : BaseFragment() {

    companion object {
        fun newInstance(product: Product): MyNutritionFragment {
            val fragment = MyNutritionFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_PRODUCT, product)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.example.yuka.R.layout.main1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        titreRecette.text = Html.fromHtml(getString(com.example.yuka.R.string.nomRecette, product.nomRecette))
        marqueRecette.text = Html.fromHtml(getString(com.example.yuka.R.string.marque, product.marque.toString().replace("[\\[\\]]", "")))
        codeBarre.text = Html.fromHtml(getString(com.example.yuka.R.string.codeBarre, product.codeBarre))
        quantite.text = Html.fromHtml(getString(com.example.yuka.R.string.quantite, product.quantite))
        lieuVente.text = Html.fromHtml(getString(com.example.yuka.R.string.lieuVente, product.lieuVente))
        allergenes.text = Html.fromHtml(getString(com.example.yuka.R.string.allergenes, product.allergenes))
        additifs.text = Html.fromHtml(getString(com.example.yuka.R.string.additifs, product.additifs))
        titreRecette.text = product.nomRecette
        Picasso.get().load(product.url).into(url)

        when (product.nutriscore) {
            "a" -> Picasso.get().load(com.example.yuka.R.drawable.nutri_score_a).into(nutriScore)
            "b" -> Picasso.get().load(com.example.yuka.R.drawable.nutri_score_b).into(nutriScore)
            "c" -> Picasso.get().load(com.example.yuka.R.drawable.nutri_score_c).into(nutriScore)
            "d" -> Picasso.get().load(com.example.yuka.R.drawable.nutri_score_d).into(nutriScore)
            "e" -> Picasso.get().load(com.example.yuka.R.drawable.nutri_score_e).into(nutriScore)
        }


        val resultIngredients = product.ingredients.toString().replace(" _", " <b>").replace("_ ", "</b> ").replace("_,", "</b> ").replace("[_", "<b>").replace("[\\[\\]]", "").replace("(_", "(<b>").replace("]", "").replace("_)", "</b>)").replace("[", "")
        var resultMarques = product.marque.toString().replace("[", "").replace("]", "")
        var resultAllergenes = product.allergenes.toString().replace("[", "").replace("]", "")
        var resultAdditifs = product.additifs.toString().replace("{", "").replace("}", "")
        ingredients.text = Html.fromHtml(getString(com.example.yuka.R.string.ingredients, resultIngredients))
        marqueRecette.text = Html.fromHtml(getString(com.example.yuka.R.string.marque, resultMarques))
        allergenes.text = Html.fromHtml(getString(com.example.yuka.R.string.allergenes, resultAllergenes))
        additifs.text = Html.fromHtml(getString(com.example.yuka.R.string.additifs, resultAdditifs))
    }
}


open class MyFicheNutritionFragment : BaseFragment() {

    companion object {
        fun newInstance(product: Product): MyFicheNutritionFragment {
            val fragment = MyFicheNutritionFragment()
            val args = Bundle()
            args.putParcelable(EXTRA_PRODUCT, product)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(com.example.yuka.R.layout.main1, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titreRecette.text = product.nomRecette
    }

}
