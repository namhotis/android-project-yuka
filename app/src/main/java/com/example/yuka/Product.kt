package com.example.yuka

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val nomRecette: String?,
    val marque: List<String?>?,
    val codeBarre: String?,
    val quantite: String? = null,
    val lieuVente: List<String?>? = null,
    val url: String?,
    val nutriscore: String?,
    var ingredients: List<String?>? = null,
    val allergenes: List<String?>? = null,
    val additifs: Map<String, String>? = null
) : Parcelable {

    fun toHistory() : History {
        return History(
            nomRecette = this.nomRecette,
            marque = this.marque,
            codeBarre = this.codeBarre,
            url = this.url,
            productNutriscore = this.nutriscore

        )
    }



}
